const Knex = require("knex");
const _ = require("lodash");
const moment = require("moment");

const knexOptions = require("../knexfile");
const {
  camelCaseObjectKeys,
  snakeCaseObjectKeys,
  ErrorMessage
} = require("../utils");

const knex = Knex(_.omit(knexOptions, ["migrations", "seeds"]));

class Card {
  constructor() {
    this.tableName = "cards";
    this.tableProperties = [
      "id",
      "name",
      "available_from",
      "available_to",
      "template_properties"
    ];
  }

  formatCardObject(card) {
    card.available_from = moment(card.available_from).format("DD/M/YY");
    card.available_to = moment(card.available_to).format("DD/M/YY");
    return camelCaseObjectKeys(card);
  }

  async getAll() {
    return knex
      .from(this.tableName)
      .select(this.tableProperties)
      .where("deleted_at", null)
      .then(rows => _.map(rows, row => this.formatCardObject(row)))
      .catch(err => {
        throw err;
      });
  }

  validateCard(card) {
    if (!card.name) throw ErrorMessage("Card name is not define");
    if (!card.availableFrom)
      throw ErrorMessage("Card availableFrom is not define");
    if (!card.availableTo) throw ErrorMessage("Card availableTo is not define");
  }

  async createCard(card) {
    this.validateCard(card);
    card = snakeCaseObjectKeys(card);

    return knex
      .from(this.tableName)
      .insert(card)
      .catch(err => {
        throw err;
      });
  }

  async getCardById(id) {
    return knex
      .from(this.tableName)
      .select(this.tableProperties)
      .where("id", id)
      .then(rows => {
        const currentCard = rows[0];
        if (!currentCard) throw ErrorMessage("No card found");
        else return this.formatCardObject(rows[0]);
      })
      .catch(err => {
        throw err;
      });
  }

  async updateCardById(id, updatedData) {
    updatedData = snakeCaseObjectKeys(updatedData);

    return knex(this.tableName)
      .where({ id })
      .update(updatedData)
      .catch(err => {
        throw err;
      });
  }

  async DeleteCardById(id) {
    return knex(this.tableName)
      .where({ id })
      .del()
      .catch(err => {
        throw err;
      });
  }
}

module.exports = new Card();
