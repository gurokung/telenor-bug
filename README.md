# Bug assignments (candidate)

Dear candidate,

Thank you for your interest in working with us at Telenor Digital Asia and taking the time to help us get familiar with your profressional skills.

This test is your opportunity to show of your technical excellence. It is an important part in
showing who you are and what you stand for.

In many aspects this test is more important than the resume you sent us. It tells us about the
choices you make as a developer, and how you solve problems presented to you.

## Guidelines

- Please use good programming practices when solving the problem.

- Please provide automated unit tests.

- Please submit your code as a link to a Git/Mercurial repository somewhere or a zip/tar.gz file with all the commit history.

- Please submit a solution that has the quality of your real work standards.

## Setup

This repo contains an API for a simple database with a single table.  You can run it with the following steps:

```bash
docker-compose up -d                # run the database
yarn                                # install dependencies
yarn run migration && yarn run seed # initialize the database
yarn start                          # run the API on port 3000
```

## Task

1. There is a bug when get call all cards (`/cards`), API return incorrect result

    - expected result

    ```json
    [
        {
            "id": 1,
            "name": "card1",
            "availableFrom": "20/2/18",
            "availableTo": "24/2/18",
            "templateProperties": {
                "textsInCard": [
                    "card1Text",
                    {
                        "textsObject": "super_text"
                    }
                ]
            }
        },
        {
            "id": 2,
            "name": "card2",
            "availableFrom": "20/2/18",
            "availableTo": "24/2/18",
            "templateProperties": {
                "feeds": [
                    {
                        "matchId": [
                            "5",
                            "6",
                            "7",
                            "8"
                        ]
                    },
                    "matchPoint"
                ]
            }
        },
        {
            "id": 3,
            "name": "card3",
            "availableFrom": "20/2/18",
            "availableTo": "24/2/18",
            "templateProperties": {
                "availableOnPlatform": {
                    "android": true,
                    "ios": true,
                    "web": true
                }
            }
        }
    ]
    ```

2. Finish CRUD (Create Retrieve Update Delete) API endpoints for `cards`

With best regards,

Telenor Digital Asia


## Testing
This project use `jasmine` for testing  
For testing please use this command
```bash
yarn test                # run jasmine test
```

Example Output
```
Started

Executing 6 defined specs...
......


6 specs, 0 failures
Finished in 0.019 seconds

>> Done!


Summary:

👊  Passed
Suites:  3 of 3
Specs:   6 of 6
Expects: 8 (0 failures)
Finished in 0.019 seconds

✨  Done in 0.30s.
```

## API Doc

### GET /cards
Show all cards
__Example response__
```json
[
    {
        "id":1,
        "name":"card1",
        "availableFrom":"20/2/18",
        "availableTo":"24/2/18",
        "templateProperties":{
            "textsInCard":[
                "card1Text",
                {
                    "textsObject":"super_text"
                }
            ]
        }
    },
    {
        "id":2,
        "name":"card2",
        "availableFrom":"20/2/18",
        "availableTo":"24/2/18",
        "templateProperties":{
            "feeds":[
                {
                    "matchId":[
                        "5",
                        "6",
                        "7",
                        "8"
                    ]
                },
                "matchPoint"
            ]
        }
    },
    {
        "id":3,
        "name":"card3",
        "availableFrom":"20/2/18",
        "availableTo":"24/2/18",
        "templateProperties":{
            "availableOnPlatform":{
                "android":true,
                "ios":true,
                "web":true
            }
        }
    }
]
```

### POST /cards
Create new card  
__Example body__ 
```json
{
    "name": "test-card",
    "availableFrom": "2018-03-12",
    "availableTo": "2018-03-15"
}
```
__Example response__
```json
Sucessfully create new card
```

### GET /cards/:cardId
Get specific card detail by given card id
__Example response__
```json
{
    "id": 1,
    "name": "card1",
    "availableFrom": "20/2/18",
    "availableTo": "24/2/18",
    "templateProperties": {
        "textsInCard": [
            "card1Text",
            {
                "textsObject": "super_text"
            }
        ]
    }
}
```

### PUT /cards/:cardId
Update card detail by given id
__Example body__ 
```json
{
    "name": "test-card-update"
}
```

__Example response__
```json
Sucessfully update card id CARD_ID
```


### DELETE /cards/:cardId
__Example response__
```json
Sucessfully delete card id CARD_ID
```