const config = require("config");
const express = require("express");
const bodyParser = require('body-parser');

const cardService = require("./services/card");

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.set("port", config.get("app.port"));

app.get("/cards", async (req, res) => {
  const cards = await cardService.getAll();

  res.json(cards);
});

/**
 * Part 2 code here
 * Finish CRUD functions for cards
 */

//  Get Card
app.get("/cards/:cardId", async (req, res) => {
  const cardId = req.params.cardId;
  try {
    const cardObj = await cardService.getCardById(cardId);
    res.json(cardObj);
  } catch (error) {
    console.error(`ERROR from GET /cards/${cardId}: ${error}`);
    res.status(400).send(error);
  }
});

//  Create Card
app.post("/cards", async (req, res) => {
  const cardObj = req.body;
  try {
    await cardService.createCard(cardObj);
    res.send("Sucessfully create new card");
  } catch (error) {
    console.error(`ERROR from POST /cards: ${error}`);
    res.status(400).send(error);
  }
});

// Update Card
app.put("/cards/:cardId", async (req, res) => {
  const cardId = req.params.cardId;
  const updatedCardObj = req.body;
  try {
    await cardService.updateCardById(cardId, updatedCardObj);
    res.send(`Sucessfully update card id ${cardId}`);
  } catch (error) {
    console.error(`ERROR from PUT /cards/${cardId}: ${error}`);
    res.status(400).send(error);
  }
});

// Delete Card
app.delete("/cards/:cardId", async (req, res) => {
  const cardId = req.params.cardId;
  try {
    await cardService.DeleteCardById(cardId);
    res.send(`Sucessfully delete card id ${cardId}`);
  } catch (error) {
    console.error(`ERROR from DELETE /cards/${cardId}: ${error}`);
    res.status(400).send(error);
  }
});

module.exports = app;
