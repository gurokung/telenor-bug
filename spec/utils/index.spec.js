const utils = require("../../utils");

describe("utils", () => {
  describe("#camelCaseObjectKeys()", () => {
    it("should return obj with camel case key", () => {
      expect(utils.camelCaseObjectKeys({ say_hello_to_me: {} })).toEqual({
        sayHelloToMe: {}
      });
    });

    it("should return obj with camel case key with nested obj", () => {
      expect(
        utils.camelCaseObjectKeys({
          say_hello_to_me: { should_go_to_sleep: 1 }
        })
      ).toEqual({ sayHelloToMe: { shouldGoToSleep: 1 } });
      expect(
        utils.camelCaseObjectKeys({
          say_hello_to_me: {
            should_go_to_sleep: 1,
            should_go_to_sleep_2: 2,
            should_go_to_sleep_3: 3
          }
        })
      ).toEqual({
        sayHelloToMe: {
          shouldGoToSleep: 1,
          shouldGoToSleep2: 2,
          shouldGoToSleep3: 3
        }
      });
    });

    it("should return obj with camel case key with nested obj with string", () => {
      expect(
        utils.camelCaseObjectKeys({
          say_hello_to_me: { should_go_to_sleep: "so sleepy" }
        })
      ).toEqual({ sayHelloToMe: { shouldGoToSleep: "so sleepy" } });
    });

    it("should return obj with camel case key with nested obj with array with string", () => {
      expect(
        utils.camelCaseObjectKeys({
          texts_in_card: [
            "card1_text",
            {
              texts_object: "super_text"
            }
          ]
        })
      ).toEqual({
        textsInCard: [
          "card1Text",
          {
            textsObject: "super_text"
          }
        ]
      });
    });
  });

  describe("#snakeCaseObjectKeys()", () => {
    it("should return obj with snake case key", () => {
      expect(utils.snakeCaseObjectKeys({ sayHelloToMe: {} })).toEqual({
        say_hello_to_me: {}
      });
    });

    it("should return obj with snake case key with nested obj", () => {
      expect(
        utils.snakeCaseObjectKeys({
          sayHelloToMe: { shouldGoToSleep: 1 }
        })
      ).toEqual({ say_hello_to_me: { should_go_to_sleep: 1 } });
      expect(
        utils.snakeCaseObjectKeys({
          sayHelloToMe: {
            shouldGoToSleep: 1,
            shouldGoToSleep2: 2,
            shouldGoToSleep3: 3
          }
        })
      ).toEqual({
        say_hello_to_me: {
          should_go_to_sleep: 1,
          should_go_to_sleep_2: 2,
          should_go_to_sleep_3: 3
        }
      });
    });
  });
});
